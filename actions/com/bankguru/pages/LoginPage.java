package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.LoginPageUI;

import commons.AbstractPage;

public class LoginPage extends AbstractPage {

	WebDriver driver;

	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}

	public RegisterPage clickHereLink() {
		if (driver.toString().toLowerCase().contains("internetexplorer")) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		waitForControlVisible(driver, LoginPageUI.HERE_LNK);
		clickToElement(driver, LoginPageUI.HERE_LNK);
		return PageFactoryPage.getRegisterPage(driver);
	}

	public String getCurrentUrl() {
		return getCurrentUrl(driver);
	}

	public void enterUserId(String userId) {
		waitForControlVisible(driver, LoginPageUI.EMAIL_TXT);
		sendKeyToElement(driver, LoginPageUI.EMAIL_TXT, userId);
	}

	public void enterPassword(String password) {
		waitForControlVisible(driver, LoginPageUI.PASSWORD_TXT);
		sendKeyToElement(driver, LoginPageUI.PASSWORD_TXT, password);

	}

	public HomePage clickSubmitBtn() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_BTN);
		clickToElement(driver, LoginPageUI.LOGIN_BTN);
		if (driver.toString().toLowerCase().contains("ie")) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return PageFactoryPage.getHomePage(driver);
	}

}
