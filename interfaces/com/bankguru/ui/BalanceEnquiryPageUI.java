package com.bankguru.ui;

public class BalanceEnquiryPageUI {

	public static final String ACC_NO_TXT = "//input[@name='accountno']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String RESET_BTN = "//input[@name='res']";
}
