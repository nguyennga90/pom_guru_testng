package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.BalEnquiryPageUI;

import commons.AbstractPage;
import commons.Constants;

public class BalEnquiryPage extends AbstractPage{

	WebDriver driver;

	public BalEnquiryPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean isMsgSuccess(String accountNo) {
		String msg = String.format(BalEnquiryPageUI.SUCCESS_TEXT, accountNo);
		return isControlIsDisplay(driver, msg);
	}

	public void assertColumnValue(String column, String value) {
		String locate = String.format(BalEnquiryPageUI.INFO_TEXT, column);
		assertTextEqual(driver, locate, value);
	}
	
	public String getAccountNo() {
		return getTextElement(driver, BalEnquiryPageUI.INFO_TEXT, Constants.ENQUIRY_ACCOUNT_NO_COL);
	}
	
	public String getTypeOfAccount() {
		return getTextElement(driver, BalEnquiryPageUI.INFO_TEXT, Constants.ENQUIRY_ACCOUNT_TYPE_COL);
	}
	
	public String getBalance() {
		return getTextElement(driver, BalEnquiryPageUI.INFO_TEXT, Constants.ENQUIRY_BALANCE_COL);
	}
	
}
