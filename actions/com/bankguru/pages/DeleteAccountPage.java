package com.bankguru.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

import com.bankguru.ui.DeleteAccountPageUI;

import commons.AbstractPage;

public class DeleteAccountPage extends AbstractPage{

	WebDriver driver;
	String msgDelete;
	Alert alert;

	public DeleteAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputAccNo(String acc) {
		waitForControlVisible(driver, DeleteAccountPageUI.ACCOUNT_NO_TXT);
		sendKeyToElement(driver, DeleteAccountPageUI.ACCOUNT_NO_TXT, acc);
	}
	
	public HomePage clickSubmitBtn() {
		waitForControlVisible(driver, DeleteAccountPageUI.SUBMIT_BTN);
		clickToElement(driver, DeleteAccountPageUI.SUBMIT_BTN);
		alert = switchToAlert(driver);
		acceptAlert(alert);
		waitForControlAlertPresence(driver);
		if (!driver.toString().toLowerCase().contains("internetexplorer")) {
			msgDelete = getTextAlert(alert);
			assertTextEqual(DeleteAccountPageUI.MSG_SUCCESS, msgDelete);
		}
		acceptAlert(alert);
		return PageFactoryPage.getHomePage(driver);
	}

	public HomePage deleteAccount(String accountNo) {
		// Input AccountID
		inputAccNo(accountNo);

		// Click Submit button
		return clickSubmitBtn();
	}

}
