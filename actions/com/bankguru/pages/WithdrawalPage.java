package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.WithdrawalInputPageUI;

import commons.AbstractPage;

public class WithdrawalPage extends AbstractPage{

	WebDriver driver;

	public WithdrawalPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputAccId(String accId) {
		waitForControlVisible(driver, WithdrawalInputPageUI.ACCOUNT_NO_TXT);
		sendKeyToElement(driver, WithdrawalInputPageUI.ACCOUNT_NO_TXT, accId);
	}
	
	public void inputAmountDebit(String amountDebit) {
		waitForControlVisible(driver, WithdrawalInputPageUI.AMOUNT_TXT);
		sendKeyToElement(driver, WithdrawalInputPageUI.AMOUNT_TXT, amountDebit);
	}
	
	public void inputDescription(String des) {
		waitForControlVisible(driver, WithdrawalInputPageUI.DESCRITION_TXT);
		sendKeyToElement(driver, WithdrawalInputPageUI.DESCRITION_TXT, des);
	}
	
	public WithdrawalInfoPage clickSubmitBtn() {
		waitForControlVisible(driver, WithdrawalInputPageUI.SUBMIT_BTN);
		clickToElement(driver, WithdrawalInputPageUI.SUBMIT_BTN);
		return PageFactoryPage.getWithdrawalInfoPage(driver);
	}
}
