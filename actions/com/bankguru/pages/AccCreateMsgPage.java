package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import commons.Constants;
import com.bankguru.ui.AccCreateMsgPageUI;

public class AccCreateMsgPage extends AbstractPage{

	WebDriver driver;

	public AccCreateMsgPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean isRegisAccountSuccess() {
		return isControlIsDisplay(driver, AccCreateMsgPageUI.SUCCESS_TEXT);
	}
	
	public String getAccountID() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_ID_COL);
	}

	public String getCustomerID() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_CUSTOMER_ID_COL);
	}
	
	public String getCustomerName() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_CUSTOMER_NAME_COL);
	}
	
	public String getEmail() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_MAIL_COL);
	}
	
	public String getAccountType() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_TYPE_COL);
	}
	
	public String getDateOfOpen() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_DATE_OPEN_COL);
	}
	
	public String getCurrentAmount() {
		return getTextElement(driver, AccCreateMsgPageUI.INFO_TEXT, Constants.NEW_ACCOUNT_CURRENT_AMOUNT_COL);
	}
}
