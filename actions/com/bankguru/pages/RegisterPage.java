package com.bankguru.pages;

import org.openqa.selenium.WebDriver;
import com.bankguru.ui.RegisterPageUI;

import commons.AbstractPage;

public class RegisterPage extends AbstractPage {
	WebDriver driver;

	public RegisterPage(WebDriver driver) {
		this.driver = driver;
	}

	public void regisNewMail(String mail) {
		waitForControlVisible(driver, RegisterPageUI.EMAIL_ID_TXT);
		sendKeyToElement(driver, RegisterPageUI.EMAIL_ID_TXT, mail);
	}

	public void clickSubmitBtn() {
		waitForControlVisible(driver, RegisterPageUI.SUBMIT_BTN);
		clickToElement(driver, RegisterPageUI.SUBMIT_BTN);
	}

	public String getUserInfo() {
		waitForControlVisible(driver, RegisterPageUI.USER_ID_TEXT);
		return getTextElement(driver, RegisterPageUI.USER_ID_TEXT);
	}

	public String getPasswordInfo() {
		waitForControlVisible(driver, RegisterPageUI.PASSWORD_ID_TEXT);
		return getTextElement(driver, RegisterPageUI.PASSWORD_ID_TEXT);
	}

	public LoginPage returnLoginPage(String url) {
		openUrl(driver, url);
		return PageFactoryPage.getLoginPage(driver);
	}
}
