package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.NewAccountPageUI;

import commons.AbstractPage;

public class NewAccountPage extends AbstractPage {

	WebDriver driver;

	public NewAccountPage(WebDriver driver) {
		this.driver = driver;
	}

	public void inputCustomerId(String customerId) {
		waitForControlVisible(driver, NewAccountPageUI.CUSTOMERID_TXT);
		sendKeyToElement(driver, NewAccountPageUI.CUSTOMERID_TXT, customerId);
	}

	public void chooseAccountType(String accountType) {
		waitForControlVisible(driver, NewAccountPageUI.ACCOUNT_TYPE_PLD);
		selectItemInDropdown(driver, NewAccountPageUI.ACCOUNT_TYPE_PLD, accountType);
	}

	public void inputDeposit(String deposit) {
		waitForControlVisible(driver, NewAccountPageUI.INITIAL_DEPOSIT_TXT);
		sendKeyToElement(driver, NewAccountPageUI.INITIAL_DEPOSIT_TXT, deposit);
	}

	public AccCreateMsgPage clickSubmitButton() {
		waitForControlVisible(driver, NewAccountPageUI.SUBMIT_BTN);
		clickToElement(driver, NewAccountPageUI.SUBMIT_BTN);
		return PageFactoryPage.getAccCreateMsgPage(driver);
	}

	public AccCreateMsgPage createAccount(String customerId, String accType, String deposit) {
		// Fill data
		inputCustomerId(customerId);
		chooseAccountType(accType);
		inputDeposit(deposit);

		// Click Submit button
		return clickSubmitButton();
	}

}
