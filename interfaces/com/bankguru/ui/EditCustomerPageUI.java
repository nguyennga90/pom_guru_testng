package com.bankguru.ui;

public class EditCustomerPageUI {

	public static final String CUSTOMER_ID_TXT = "//input[@name='cusid']";
	public static final String SUBMIT_BTN = "//input[@name='AccSubmit']";
	public static final String RESET_BTN = "//input[@name='res']";
}
