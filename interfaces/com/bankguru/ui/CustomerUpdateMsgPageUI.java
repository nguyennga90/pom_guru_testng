package com.bankguru.ui;

public class CustomerUpdateMsgPageUI {

	public static final String SUCCESS_TEXT = "//p[contains(text(),'Customer details updated Successfully!!!')]";
	public static final String INFO_TEXT = "//table[@id='customer']//td[contains(text(), '%s')]//following-sibling::td";

}
