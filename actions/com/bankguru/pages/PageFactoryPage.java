package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

public class PageFactoryPage {

	private static LoginPage loginPage;
	private static RegisterPage registerPage;
	private static HomePage homePage;
	private static NewCustomerPage newCustomerPage;
	private static EditCustomerPage editCustomerPage;
	private static DeleteCustomerPage deleteCustomerPage;
	private static NewAccountPage newAccountPage;
	private static EditAccountPage editAccountPage;
	private static DeleteAccountPage deleteAccountPage;
	private static DepositPage depositPage;
	private static WithdrawalPage withdrawalPage;
	private static FundTransferPage fundTransferPage;
	private static ChangePasswordPage changePasswordPage;
	private static BalanceEnquiryPage balanceEnquiryPage;
	private static MiniStatementPage miniStatementPage;
	private static CustomisedStatementPage customisedStatementPage;
	private static LogOutPage logOutPage;
	private static CustomerRegMsgPage customerRegMsgPage;
	private static EditDetailCustomerPage editDetailCustomerPage;
	private static CustomerUpdateMsgPage customerUpdateMsgPage;
	private static AccCreateMsgPage accCreateMsgPage;
	private static DepositInfoPage depositInfoPage;
	private static WithdrawalInfoPage withdrawalInfoPage;
	private static FundTransPage fundTransPage;
	private static BalEnquiryPage balEnquiryPage;

	public static LoginPage getLoginPage(WebDriver driver) {
		if (loginPage == null) {
			return new LoginPage(driver);
		} else {
			return loginPage;
		}
	}

	public static RegisterPage getRegisterPage(WebDriver driver) {
		if (registerPage == null) {
			return new RegisterPage(driver);
		} else {
			return registerPage;
		}
	}

	public static HomePage getHomePage(WebDriver driver) {
		if (homePage == null) {
			return new HomePage(driver);
		} else {
			return homePage;
		}
	}

	public static NewCustomerPage getNewCustomerPage(WebDriver driver) {
		if (newCustomerPage == null) {
			return new NewCustomerPage(driver);
		} else {
			return newCustomerPage;
		}
	}

	public static EditCustomerPage getEditCustomerPage(WebDriver driver) {
		if (editCustomerPage == null) {
			return new EditCustomerPage(driver);
		} else {
			return editCustomerPage;
		}
	}

	public static DeleteCustomerPage getDeleteCustomerPage(WebDriver driver) {
		if (deleteCustomerPage == null) {
			return new DeleteCustomerPage(driver);
		} else {
			return deleteCustomerPage;
		}
	}

	public static NewAccountPage getNewAccountPage(WebDriver driver) {
		if (newAccountPage == null) {
			return new NewAccountPage(driver);
		} else {
			return newAccountPage;
		}
	}

	public static EditAccountPage getEditAccountPage(WebDriver driver) {
		if (editAccountPage == null) {
			return new EditAccountPage(driver);
		} else {
			return editAccountPage;
		}
	}

	public static DeleteAccountPage getDeleteAccountPage(WebDriver driver) {
		if (deleteAccountPage == null) {
			return new DeleteAccountPage(driver);
		} else {
			return deleteAccountPage;
		}
	}

	public static DepositPage getDepositPage(WebDriver driver) {
		if (depositPage == null) {
			return new DepositPage(driver);
		} else {
			return depositPage;
		}
	}

	public static WithdrawalPage getWithdrawalPage(WebDriver driver) {
		if (withdrawalPage == null) {
			return new WithdrawalPage(driver);
		} else {
			return withdrawalPage;
		}
	}

	public static FundTransferPage getFundTransferPage(WebDriver driver) {
		if (fundTransferPage == null) {
			return new FundTransferPage(driver);
		} else {
			return fundTransferPage;
		}
	}

	public static ChangePasswordPage getChangePasswordPage(WebDriver driver) {
		if (changePasswordPage == null) {
			return new ChangePasswordPage(driver);
		} else {
			return changePasswordPage;
		}
	}

	public static BalanceEnquiryPage getBalanceEnquiryPage(WebDriver driver) {
		if (balanceEnquiryPage == null) {
			return new BalanceEnquiryPage(driver);
		} else {
			return balanceEnquiryPage;
		}
	}

	public static MiniStatementPage getMiniStatementPage(WebDriver driver) {
		if (miniStatementPage == null) {
			return new MiniStatementPage(driver);
		} else {
			return miniStatementPage;
		}
	}

	public static CustomisedStatementPage getCustomisedStatementPage(WebDriver driver) {
		if (customisedStatementPage == null) {
			return new CustomisedStatementPage(driver);
		} else {
			return customisedStatementPage;
		}
	}

	public static LogOutPage getLogOutPage(WebDriver driver) {
		if (logOutPage == null) {
			return new LogOutPage(driver);
		} else {
			return logOutPage;
		}
	}

	public static CustomerRegMsgPage getCustomerRegMsgPage(WebDriver driver) {
		if (customerRegMsgPage == null) {
			return new CustomerRegMsgPage(driver);
		} else {
			return customerRegMsgPage;
		}
	}

	public static EditDetailCustomerPage getEditDetailCustomerPage(WebDriver driver) {
		if (editDetailCustomerPage == null) {
			return new EditDetailCustomerPage(driver);
		} else {
			return editDetailCustomerPage;
		}
	}

	public static CustomerUpdateMsgPage getCustomerUpdateMsgPage(WebDriver driver) {
		if (customerUpdateMsgPage == null) {
			return new CustomerUpdateMsgPage(driver);
		} else {
			return customerUpdateMsgPage;
		}
	}

	public static AccCreateMsgPage getAccCreateMsgPage(WebDriver driver) {
		if (accCreateMsgPage == null) {
			return new AccCreateMsgPage(driver);
		} else {
			return accCreateMsgPage;
		}
	}

	public static DepositInfoPage getDepositInfoPage(WebDriver driver) {
		if (depositInfoPage == null) {
			return new DepositInfoPage(driver);
		} else {
			return depositInfoPage;
		}
	}

	public static WithdrawalInfoPage getWithdrawalInfoPage(WebDriver driver) {
		if (withdrawalInfoPage == null) {
			return new WithdrawalInfoPage(driver);
		} else {
			return withdrawalInfoPage;
		}
	}

	public static FundTransPage getFundTransPage(WebDriver driver) {
		if (fundTransPage == null) {
			return new FundTransPage(driver);
		} else {
			return fundTransPage;
		}
	}

	public static BalEnquiryPage getBalEnquiryPage(WebDriver driver) {
		if (balEnquiryPage == null) {
			return new BalEnquiryPage(driver);
		} else {
			return balEnquiryPage;
		}
	}
}
