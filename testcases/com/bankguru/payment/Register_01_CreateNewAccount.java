package com.bankguru.payment;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.bankguru.pages.AccCreateMsgPage;
import com.bankguru.pages.BalEnquiryPage;
import com.bankguru.pages.BalanceEnquiryPage;
import com.bankguru.pages.CustomerRegMsgPage;
import com.bankguru.pages.CustomerUpdateMsgPage;
import com.bankguru.pages.DeleteAccountPage;
import com.bankguru.pages.DeleteCustomerPage;
import com.bankguru.pages.DepositInfoPage;
import com.bankguru.pages.DepositPage;
import com.bankguru.pages.EditCustomerPage;
import com.bankguru.pages.EditDetailCustomerPage;
import com.bankguru.pages.FundTransPage;
import com.bankguru.pages.FundTransferPage;
import com.bankguru.pages.HomePage;
import com.bankguru.pages.LoginPage;
import com.bankguru.pages.NewAccountPage;
import com.bankguru.pages.NewCustomerPage;
import com.bankguru.pages.PageFactoryPage;
import com.bankguru.pages.RegisterPage;
import com.bankguru.pages.WithdrawalInfoPage;
import com.bankguru.pages.WithdrawalPage;

import commons.AbstractTest;

public class Register_01_CreateNewAccount extends AbstractTest {
	WebDriver driver;
	private String loginURL, userId, password, mail, mailCustomer, customerID, mailCustomerEdit, accountNo,
			currentBalance, payeeAccountNo, accountType, strTransferDes1, strWithdrawalDes, strTransferDes2;
	private String deposit, depositAmount, amountDebit, transferAmount;
	// customer info
	private String strNewCustomerName, strNewCustomerBirthday, strNewCustomerAddress, strNewCustomerCity,
			strNewCustomerState, strNewCustomerPin, strNewCustomerPhoneNo, strNewCustomerPass;
	// edit customer
	private String strEditCustomerAddress, strEditCustomerCity, strEditCustomerState, strEditCustomerPin,
			strEditCustomerPhoneNo;
	private LoginPage loginPage;
	private RegisterPage registerPage;
	private HomePage homePage;
	private NewCustomerPage newCustomerPage;
	private CustomerRegMsgPage customerRegMsgPage;
	private EditCustomerPage editCustomerPage;
	private EditDetailCustomerPage editDetailCustomerPage;
	private CustomerUpdateMsgPage customerUpdateMsgPage;
	private NewAccountPage newAccountPage;
	private AccCreateMsgPage accCreateMsgPage;
	private DepositPage depositPage;
	private DepositInfoPage depositInfoPage;
	private WithdrawalPage withdrawalPage;
	private WithdrawalInfoPage withdrawalInfoPage;
	private FundTransferPage fundTransferPage;
	private FundTransPage fundTransPage;
	private BalanceEnquiryPage balanceEnquiryPage;
	private BalEnquiryPage balEnquiryPage;
	private DeleteAccountPage deleteAccountPage;
	private DeleteCustomerPage deleteCustomerPage;

	@Parameters({ "browser", "url", "version" })
	@BeforeClass
	public void beforeClass(String browser, String url, @Optional("firefox") String version) {
		driver = openMultiBrowser(browser, url, version);
		mail = "ngantt" + randomNumber() + "@gmail.com";
		// New Customer
		strNewCustomerName = "AUTOMATION TESTING";
		strNewCustomerBirthday = "01/01/1989";
		strNewCustomerAddress = "PO Box 911 8331 Duis Avenue";
		strNewCustomerCity = "Tampa";
		strNewCustomerState = "FL";
		strNewCustomerPin = "466250";
		strNewCustomerPhoneNo = "4555442476";
		mailCustomer = "nganttauto" + randomNumber() + "@gmail.com";
		strNewCustomerPass = "automation";

		// Edit Customer
		strEditCustomerAddress = "1883 Cursus Avenue";
		strEditCustomerCity = "Houston";
		strEditCustomerState = "Texas";
		strEditCustomerPin = "166455";
		strEditCustomerPhoneNo = "4779728081";
		mailCustomerEdit = "ngantttest" + randomNumber() + "@gmail.com";

		// Amount
		deposit = "50000";
		depositAmount = "5000";
		amountDebit = "15000";
		transferAmount = "10000";

		// Account
		accountType = "Current";

		// Description
		strTransferDes1 = "Deposit";
		strWithdrawalDes = "Withdraw";
		strTransferDes2 = "Transfer";

	}

	@Test
	public void Register_01_CreateNewCustomer() {

		// Create account
		loginPage = PageFactoryPage.getLoginPage(driver);
		loginURL = loginPage.getCurrentUrl();
		registerPage = loginPage.clickHereLink();

		registerPage.regisNewMail(mail);
		registerPage.clickSubmitBtn();
		userId = registerPage.getUserInfo();
		password = registerPage.getPasswordInfo();

		// Login
		loginPage = registerPage.returnLoginPage(loginURL);

		loginPage.enterUserId(userId);
		loginPage.enterPassword(password);
		homePage = loginPage.clickSubmitBtn();

		// Open New Customer Page
		newCustomerPage = homePage.openNewCustomerPage(driver);

		// Fill data
		newCustomerPage.inputCustomerName(strNewCustomerName);
		newCustomerPage.chooseGenderFemale();
		newCustomerPage.inputBirthday(strNewCustomerBirthday);
		newCustomerPage.inputAddress(strNewCustomerAddress);
		newCustomerPage.inputCity(strNewCustomerCity);
		newCustomerPage.inputState(strNewCustomerState);
		newCustomerPage.inputPin(strNewCustomerPin);
		newCustomerPage.inputPhoneNo(strNewCustomerPhoneNo);
		newCustomerPage.inputEmail(mailCustomer);
		newCustomerPage.inputPass(strNewCustomerPass);

		// submit
		customerRegMsgPage = newCustomerPage.clickSubmitBtn();

		// verify
		verifyTrue(customerRegMsgPage.isRegisCustomerSuccess());
		customerID = customerRegMsgPage.getCustomerID();
	}

	@Test
	public void Register_02_EditCustomer() {
		// Open Edit Customer page
		editCustomerPage = customerRegMsgPage.openEditCustomerPage(driver);

		// Enter CustomerID field
		editCustomerPage.inputCustomerID(customerID);

		// Click Submit button
		editDetailCustomerPage = editCustomerPage.clickSubmitBtn();

		// Fill data
		editDetailCustomerPage.changeAddress(strEditCustomerAddress);
		editDetailCustomerPage.changeCity(strEditCustomerCity);
		editDetailCustomerPage.changeState(strEditCustomerState);
		editDetailCustomerPage.changePin(strEditCustomerPin);
		editDetailCustomerPage.changePhoneNo(strEditCustomerPhoneNo);
		editDetailCustomerPage.changeMail(mailCustomerEdit);

		// Click Submit button
		customerUpdateMsgPage = editDetailCustomerPage.clickSubmitBtn();

		// verify
		verifyTrue(customerUpdateMsgPage.isUpdateCustomerSuccess());
	}

	@Test
	public void Register_03_CreateAccount() {
		// Open New Account page
		newAccountPage = customerUpdateMsgPage.openNewAccountPage(driver);

		// Create new account
		accCreateMsgPage = newAccountPage.createAccount(customerID, accountType, deposit);

		// verify
		verifyTrue(accCreateMsgPage.isRegisAccountSuccess());
		verifyEquals(accCreateMsgPage.getCurrentAmount(), deposit);

		accountNo = accCreateMsgPage.getAccountID();
	}

	@Test
	public void Register_04_TransferMoney_CurrentAccount() {
		// Open Deposit page
		depositPage = accCreateMsgPage.openDepositPage(driver);

		// Fill data
		depositPage.inputAccId(accountNo);
		depositPage.inputAmount(depositAmount);
		depositPage.inputDescription(strTransferDes1);

		// Click Submit button
		depositInfoPage = depositPage.clickSubmitBtn();

		// Verify
		verifyTrue(depositInfoPage.isMsgDeposit(accountNo));
		currentBalance = String.valueOf(Integer.parseInt(deposit) + Integer.parseInt(depositAmount));
		verifyEquals(depositInfoPage.getCurrentBalance(), currentBalance);
	}

	@Test
	public void Register_05_WithdrawMoney() {
		// Open Withdrawal page
		withdrawalPage = depositInfoPage.openWithdrawalPage(driver);

		// Fill data
		withdrawalPage.inputAccId(accountNo);
		withdrawalPage.inputAmountDebit(amountDebit);
		withdrawalPage.inputDescription(strWithdrawalDes);

		// Click Submit button
		withdrawalInfoPage = withdrawalPage.clickSubmitBtn();

		// Verify
		verifyTrue(withdrawalInfoPage.isMsgWithdrawal(accountNo));
		currentBalance = String.valueOf(Integer.parseInt(currentBalance) - Integer.parseInt(amountDebit));
		verifyEquals(withdrawalInfoPage.getCurrentBalance(), currentBalance);
	}

	@Test
	public void Register_06_TransferMoney_OtherAccount() {
		// Open New Account page
		newAccountPage = withdrawalInfoPage.openNewAccountPage(driver);

		// Create new account
		accCreateMsgPage = newAccountPage.createAccount(customerID, accountType, deposit);

		payeeAccountNo = accCreateMsgPage.getAccountID();

		// Open Fund Transfer page
		fundTransferPage = accCreateMsgPage.openFundTransferPage(driver);

		// Transfer to other account
		fundTransPage = fundTransferPage.transferToOtherAcc(accountNo, payeeAccountNo, transferAmount, strTransferDes2);

		// Verify
		verifyEquals(fundTransPage.getFromAccount(), accountNo);
		verifyEquals(fundTransPage.getAmount(), transferAmount);
	}

	@Test
	public void Register_07_CheckCurrentAccBalance() {
		// Open Balance Enquiry page
		balanceEnquiryPage = fundTransPage.openBalanceEnquiryPage(driver);

		// Fill in Account No field
		balanceEnquiryPage.inputAccNo(accountNo);

		// Click Submit button
		balEnquiryPage = balanceEnquiryPage.clickSubmitBtn();

		// Verify
		verifyTrue(balEnquiryPage.isMsgSuccess(accountNo));
		currentBalance = String.valueOf(Integer.parseInt(currentBalance) - Integer.parseInt(transferAmount));
		verifyEquals(balEnquiryPage.getBalance(), currentBalance);
	}

	@Test
	public void Register_08_DeleteAllAccount() {
		// Open Delete Account page
		deleteAccountPage = balanceEnquiryPage.openDeleteAccountPage(driver);

		// Delete payer account
		log.info("delete account1");
		homePage = deleteAccountPage.deleteAccount(accountNo);

		// Open Delete Account page
		log.info("delete account2");
		deleteAccountPage = homePage.openDeleteAccountPage(driver);

		// Delete payee account
		homePage = deleteAccountPage.deleteAccount(payeeAccountNo);
	}

	@Test
	public void Register_09_DeleteCustomer() {
		// Open Delete Account page
		deleteCustomerPage = homePage.openDeleteCustomerPage(driver);

		// Delete customer
		homePage = deleteCustomerPage.deleteCustomer(customerID);
	}

	@AfterClass
	public void afterClass() {
		closeBrowser();
	}

}
