package com.bankguru.pages;

import org.openqa.selenium.WebDriver;

import com.bankguru.ui.EditDetailCustomerPageUI;

import commons.AbstractPage;

public class EditDetailCustomerPage extends AbstractPage{

	WebDriver driver;

	public EditDetailCustomerPage(WebDriver driver) {
		this.driver = driver;
	}	
	
	public void changeAddress(String address) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.ADDRESS_TXA);
		sendKeyToElement(driver, EditDetailCustomerPageUI.ADDRESS_TXA, address);
	}
	
	public void changeCity(String city) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.CITY_TXT);
		sendKeyToElement(driver, EditDetailCustomerPageUI.CITY_TXT, city);
	}
	
	public void changeState(String state) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.STATE_TXT);
		sendKeyToElement(driver, EditDetailCustomerPageUI.STATE_TXT, state);
	}
	
	public void changePin(String pin) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.PIN_TXT);
		sendKeyToElement(driver, EditDetailCustomerPageUI.PIN_TXT, pin);
	}
	
	public void changePhoneNo(String phone) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.PHONE_NO_TXT);
		sendKeyToElement(driver, EditDetailCustomerPageUI.PHONE_NO_TXT, phone);
	}

	public void changeMail(String mail) {
		waitForControlVisible(driver, EditDetailCustomerPageUI.MAIL_TXT);
		sendKeyToElement(driver, EditDetailCustomerPageUI.MAIL_TXT, mail);
	}
	
	public CustomerUpdateMsgPage clickSubmitBtn() {
		waitForControlVisible(driver, EditDetailCustomerPageUI.SUBMIT_BTN);
		clickToElement(driver, EditDetailCustomerPageUI.SUBMIT_BTN);
		return PageFactoryPage.getCustomerUpdateMsgPage(driver);
	}
}
