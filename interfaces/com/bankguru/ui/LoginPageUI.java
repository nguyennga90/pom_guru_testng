package com.bankguru.ui;

public class LoginPageUI {

	public static final String EMAIL_TXT = "//input[@name='uid']";
	public static final String PASSWORD_TXT = "//input[@name='password']";
	public static final String LOGIN_BTN = "//input[@name='btnLogin']";
	public static final String RESET_BTN = "//input[@name='btnReset']";
	public static final String HERE_LNK = "//a[contains(text(),'here')]";
	
}
